import { Component ,OnDestroy,OnInit,OnChanges} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,OnDestroy {

  isOpen : boolean = false;

  numbers: number[] = [1,2,5,14,74,3,89,48];

  stringa:string="";

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    //utilizzando quando il componente viene distrutto
    this.stringa = "";
  }


  constructor(){
    console.log("Costruttore")
    this.stringa = "sono la stringa del costruttore";
  }
 
}
