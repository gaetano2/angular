import { NomeCompleto } from "./nome-completo";
import { DataDiNascita } from "./data-di-nascita"
export interface Persona {
    nomeCompleto:NomeCompleto;
    dataDiNascita:DataDiNascita;
}
